import { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, FlatList, ScrollView } from 'react-native';

const TodoApp = () => {
  const [todos, setTodos] = useState([]); // Состояние для списка дел
  const [newTodo, setNewTodo] = useState(''); // Состояние для нового элемента списка
  const [isEditing, setIsEditing] = useState(false);
  const [editingElement, setEditingElement] = useState('');

  // Функция для добавления нового дела в список
  const addTodo = () => {
    if (newTodo !== '') {
      console.log(todos.length)
      setTodos([...todos, { id: Date.now(), text: newTodo, position:todos.length }]);
      setNewTodo('');
    }
  };

  // Функция для удаления дела из списка
  const removeTodo = (id) => {
    const updatedTodos = todos.filter((todo) => todo.id !== id);
    setTodos(updatedTodos);
  };

  

  const changeTodo = (item) => {
    setEditingElement(item)
    setNewTodo(item.text);
    setIsEditing(true)
  }

  const saveChanges = () => {
    todos[editingElement.position].text = newTodo
    setIsEditing(false)
    setNewTodo('');
    setEditingElement('')
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Todo List</Text>

      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Enter a new todo"
          value={newTodo}
          onChangeText={(text) => setNewTodo(text)}
        />
        <TouchableOpacity style={styles.addButton} onPress={addTodo}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      </View>
      {isEditing ? (
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Измените туду"
            value={newTodo}
            onChangeText={(text) => setNewTodo(text)}
          />
          <TouchableOpacity style={styles.addButton} onPress={saveChanges}>
            <Text style={styles.buttonText}>Сохранить</Text>
          </TouchableOpacity>
        </View>
      ) : (
      <FlatList
        style={styles.list}
        data={todos}
        renderItem={({ item }) => (
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.todoItem}>
              <Text style={styles.textitem}>{item.text}</Text>
              <Text style={styles.textitem}>{ new Date(item.id).toString()} Дата выполнения</Text>
              <TouchableOpacity onPress={() => changeTodo(item)}>
                <Text style={styles.changeButton}>Изменить туду</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => removeTodo(item.id)}>
                <Text style={styles.removeButton}>X</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
      )}
      

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  textitem:{
    marginRight:5
  },
  changeButton :{
    color: 'green',
    fontWeight: 'bold',
    marginRight:5,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  inputContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  input: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginRight: 10,
  },
  addButton: {
    backgroundColor: 'blue',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  list: {
    flex: 1,
  },
  todoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  removeButton: {
    color: 'red',
    fontWeight: 'bold',
  },
});

export default TodoApp;